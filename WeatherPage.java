package lecturenotes.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WeatherPage {
	/*
	 * path of search bar icon
	 */
	@FindBy(xpath=("//input[@class='search-input']"))private WebElement search;
	/*
	 * path of More detail link
	 */
	@FindBy(xpath=("//div[@class='spaced-content']"))private WebElement detail;
	/*
	 * path of Next arrow mark button
	 */
	@FindBy(xpath=("//a[@href='/en/in/odisha/2908216/daily-weather-forecast/2908216?day=2']"))private WebElement next;
	
	

	public WebElement getsearch() {
		return search;
	}
	public void setsearch(String sc) {
		search.sendKeys(sc);
	}
	
	public WebElement getdetail() {
		return detail;
	}
	public void clickdetail() {
		detail.click();
	}
	
	
	
	public WebElement getnext() {
		return next;
	}
	public void clicknext() {
		next.click();
	}
	
	
	
	public WeatherPage (WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void TC1(String sc ) throws Throwable {
		//click on about
		search.sendKeys(sc);
		detail.click();
		next.click();
	
	}
	
	
}
