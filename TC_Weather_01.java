package lecturenotes.Tests;

import org.testng.annotations.Test;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Listeners;


import lecturenotes.generic.FileLib;
import lecturenotes.generic.WebdriverCommonLib;
import lecturenotes.pages.WeatherPage;

@Listeners(lecturenotes.generic.MyListener.class)
public class TC_Weather_01 extends lecturenotes.generic.BaseTest {
	
	
	FileLib flib;
	WebdriverCommonLib wlib;
	WeatherPage weat;
	
	@Test
	
	public void TestWeather() throws Throwable {
		
	wlib = new WebdriverCommonLib();
	weat =new WeatherPage(driver);
	flib=new FileLib();
	/*
	 * Enter the URL
	 * Verify the URL
	 */
	Thread.sleep(3000);
	flib.getPropkeyValue(PROP_PATH, "url");
	wlib.verifyurl(driver.getCurrentUrl(), "https://www.accuweather.com", "Weather page ");
	/*
	 * maximize the window
	 */
	driver.manage().window().maximize();
	Thread.sleep(2000);
	/*
	 * Search the weather location
	 * Click on Enter button
	 */
	weat.setsearch("Odisha");
	Reporter.log("Entered location is Odisha");
	Thread.sleep(2000);
	Robot r = new Robot();
	r.keyPress(KeyEvent.VK_ENTER);
	r.keyRelease(KeyEvent.VK_ENTER);
	/*
	 * click on more details
	 */
	Thread.sleep(5000);
	//((JavascriptExecutor)driver).executeScript("scroll(0,200)");
	weat.clickdetail();
	Thread.sleep(2000);
	/*
	 * Remove unexpected popup
	 */
	r.keyPress(KeyEvent.VK_ENTER);
	r.keyRelease(KeyEvent.VK_ENTER);
	Thread.sleep(2000);
	Reporter.log("detail is displaying");
	/*
	 * display the tomorrow weather Report
	 */
	weat.clicknext();
	
	/*
	 * Print the weather detail of the particular date
	 */
	
	String test1 = driver.findElement(By.xpath("//body[@class='daily-detail-daynight']")).getText();
	Reporter.log("The details of tomorrow weather details  " +  test1);
	System.out.println("The details of tomorrow weather details  " +  test1);
	Thread.sleep(2000);
	/*
	 * close the browser
	 */
	driver.close();
	}
	
	
	
}
