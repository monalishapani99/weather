package lecturenotes.generic;

import java.io.FileInputStream;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;



public class FileLib {
	
		public int getRowCount(String path,String Sheet) throws Throwable{
			FileInputStream fis = new FileInputStream(path);
			Workbook wb = WorkbookFactory.create(fis);
			 int rowcount = wb.getSheet(Sheet).getLastRowNum();
			return rowcount;
		}
		
		public String getPropkeyValue(String propPath,String key) throws Throwable {
			 FileInputStream fis = new FileInputStream(propPath);
			 Properties prop = new Properties();
			 prop.load(fis);
			 String propValue = prop.getProperty(key, "not a value");
			 return propValue;


		}
		
		
		


}
