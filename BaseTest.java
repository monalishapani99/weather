package lecturenotes.generic;



import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;



public class BaseTest implements IAutoConsts {
	public static WebDriver driver;
	FileLib flib;
	
	
	@BeforeClass
	public void setUp() throws Throwable 
	 { 
		FileLib flib = new FileLib();
		String browserValue = flib.getPropkeyValue(PROP_PATH,"browser");
		if(browserValue.equalsIgnoreCase("chrome")) {
			System.setProperty(CHROME_KEY, CHROME_VALUE);
			/*
			 * block the chrome browser notification
			 */
			ChromeOptions options=new ChromeOptions();
			Map<String, Object> prefs=new HashMap<String,Object>();
			prefs.put("profile.default_content_setting_values.notifications", 2);
			options.setExperimentalOption("prefs",prefs);
			 driver=new ChromeDriver(options);
			
		}
		else if(browserValue.equalsIgnoreCase("firefox")) {
			System.setProperty(GECKO_KEY, GECKO_VALUE);
			driver=new FirefoxDriver();
		}
		else {
			System.out.println("Enter correct browser name");
		}
		/*
		 * Enter the URL of the page
		 */
		driver.get(flib.getPropkeyValue(PROP_PATH,"url"));
		
	 }
}


	 	
	

